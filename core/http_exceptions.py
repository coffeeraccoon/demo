import json


class ValidationError(Exception):
    def __init__(self):
        super(ValidationError, self).__init__("Validation error")
        self.error_code = 0
        self.error_message = "Validation error"

    def as_json(self):
        error = dict()
        error["code"] = self.error_code
        error["message"] = self.error_message
        error["success"] = False
        return json.dumps(error)


class FailedToParseException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 1001
        self.error_message = "Failed to parse data"


class FailedToDecodeException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 1002
        self.error_message = "Failed to decode data"


class PageNotFoundException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4002
        self.error_message = "Page is not found"


class URLAlreadyExistException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4003
        self.error_message = "URL is already exist"


class FileNotFoundException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4004
        self.error_message = "File not found"


class FileAlreadyExistException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4005
        self.error_message = "File is already exist"


class IncorrectFileExtensionException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4006
        self.error_message = "Incorrect file extension"


class IncorrectFileSizeException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4007
        self.error_message = "Incorrect file size"


class DirNotFoundException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4008
        self.error_message = "Directory not found"


class DirAlreadyExistException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4009
        self.error_message = "Directory is already exist"


class IncorrectReplacePath(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4010
        self.error_message = "Incorrect path for replacing"


class AttemptToDeleteRoot(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4011
        self.error_message = "Attempt to delete root"


class AttemptToEditRoot(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4012
        self.error_message = "Attempt to edit root"


class NoAccessToResourceException(ValidationError):
    def __init__(self):
        super(ValidationError, self).__init__()
        self.error_code = 4013
        self.error_message = "No access to resource"
