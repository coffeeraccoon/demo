from cms_apps.core import exceptions

class BaseRule:
    def __init__(self):
        pass

    async def check_rule(self):
        return True


class BasePermission:
    __ruleclass__ = BaseRule
    rules = list()

    def __init__(self):
        pass

    def add_rule(self, rule):
        if not isinstance(rule, self.__ruleclass__):
            raise exceptions.IncorrectClassException("Rule must be instance of <Rule> class")
        self.rules.append(rule)

    def clear(self):
        self.rules = list()

    async def check_permission(self):
        for rule in self.rules:
            if not await rule.check_rule():
                return False
        return True
