class Url(object):
    def __init__(self, method, route, view, name=None):
        self.method = method
        self.route = route
        self.view = view
        self.name = name


def url(method, route, view, name=None):
    result_url = Url(method, route, view, name)
    return result_url


def multiurl(route, view):
    result_urls = list()
    for handler in view.get_handlers():
        result_urls.append(Url(method=handler["method"],
                               route=route,
                               view=handler["view"]))
    return result_urls
