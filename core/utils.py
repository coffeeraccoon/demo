import json
import os

from aiohttp import web

from cms_apps import settings


# Function, which return json-formatted string with info about http error by error code and message
def error_as_json(code, message):
    error = dict()
    error["code"] = code
    error["message"] = message
    error["success"] = False
    return json.dumps(error)


# Decorator to a view, which execute view only if user is authenticated, otherwise return 401 with info
def auth_required(view):
    async def wrapper(*args):
        peer_name = args[-1].transport.get_extra_info('peername')
        if peer_name is not None:
            host, port = peer_name
            if host not in settings.WHITELIST_IP:
                response = error_as_json(401, "Unauthorized")
                return web.HTTPUnauthorized(text=response, content_type="application/json")
        else:
            response = error_as_json(401, "Unauthorized")
            return web.HTTPUnauthorized(text=response, content_type="application/json")
        x_api_key = args[-1].headers["X-API-KEY"] if "X-API-KEY" in args[-1].headers else ""
        if x_api_key == settings.X_API_KEY:
            response = await view(*args)
            return response
        else:
            response = error_as_json(401, "Unauthorized")
            return web.HTTPUnauthorized(text=response, content_type="application/json")
    return wrapper


def get_content_type(name):
    content_types = {
        ".jpg": "image/jpeg",
        ".jpeg": "image/jpeg",
        ".png": "image/png",
        ".gif": "image/gif",
        ".js": "text/javascript",
        ".css": "text/css",
        ".html": "text/html",
        ".htm": "text/html"
    }
    ext = get_extension(name)
    if ext == "":
        return "text/plain"
    if ext in content_types:
        return content_types[ext]
    else:
        return "text/plain"


def get_extension(name):
    dot = name.rfind(".")
    if dot == -1:
        return ""
    return name[dot:]


def remove_file(path):
    if not os.path.isfile(path):
        return
    os.remove(path)


def is_binary(ext):
    binary = [".jpg",
              ".jpeg",
              ".png",
              ".gif"]
    if ext in binary:
        return True
    else:
        return False
