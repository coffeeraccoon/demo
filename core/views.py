from cms_apps.core import http
from cms_apps.core import http_exceptions
from cms_apps.core import exceptions
from cms_apps.core import permissions
from cms_apps.core import serializers
from cms_apps.core import validators
from aiohttp import web


class BaseView:
    __serializer__ = serializers.BaseSerializer
    __validator__ = validators.BaseValidator
    __permission__ = permissions.BasePermission

    serialized_data = dict()
    validated_data = dict()

    def __init__(self):
        if isinstance(self.__serializer__, serializers.BaseSerializer):
            raise exceptions.IncorrectInheritance("__serializer__ needs to inherit from BaseSerializer")

    def get_handlers(self):
        handlers = list()
        for method in http.HTTP_METHOD_NAMES:
            if hasattr(self, method) and callable(getattr(self, method)):
                handler = dict()
                handler["method"] = method
                handler["view"] = self.as_cls_view(method)
                handlers.append(handler)
        return handlers

    @classmethod
    def as_cls_view(cls, method):
        async def handler(request):
            view = cls()
            # Serialize request data
            serializer = getattr(view.__serializer__, method)
            view.serialized_data = await serializer(request)
            # Validate data
            validator = getattr(view.__validator__, method)
            try:
                view.validated_data = await validator(request, view.serialized_data)
            except http_exceptions.ValidationError as e:
                return web.HTTPBadRequest(text=e.as_json(), content_type="application/json")
            # Check for permission
            if "permission_rules" in view.validated_data:
                perm = view.__permission__()
                for rule in view.validated_data["permission_rules"]:
                    perm.add_rule(rule)
                if not await perm.check_permission():
                    e = http_exceptions.NoAccessToResourceException()
                    return web.HTTPBadRequest(text=e.as_json(), content_type="application/json")
            # Run handler method and return the response
            h = getattr(view, method)
            return await h(request)
        return handler
