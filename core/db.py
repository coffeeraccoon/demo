import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from cms_apps import settings


BaseModel = declarative_base()
metadata = BaseModel.metadata
engine = sa.create_engine("{:s}://{:s}:{:s}@{:s}:{:s}/{:s}".format(settings.DB_DRIVER,
                                                                   settings.DB_USER,
                                                                   settings.DB_PASS,
                                                                   settings.DB_HOST,
                                                                   settings.DB_PORT,
                                                                   settings.DB_NAME),
                          echo=True if settings.DB_DEBUG else False)
Session = sessionmaker(bind=engine)
