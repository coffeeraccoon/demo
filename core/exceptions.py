class NoAncestorException(Exception):
    pass


class IncorrectClassException(Exception):
    pass


class IncorrectInheritance(Exception):
    pass
