# Get the app and iterable struct with server.core.Url items
def create_routing(app, urls):
    for url in urls:
        if isinstance(url, list):
            for u in url:
                app.router.add_route(u.method, u.route, u.view)
        else:
            if url.name is None:
                app.router.add_route(url.method, url.route, url.view)
            else:
                app.router.add_route(url.method, url.route, url.view,  name=url.name)
