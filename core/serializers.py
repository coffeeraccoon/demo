class BaseSerializer:
    def __init__(self):
        pass

    @staticmethod
    async def get(request):
        return dict(request.GET)

    @staticmethod
    async def post(request):
        return dict(await request.post())

    @staticmethod
    async def put(request):
        return dict(await request.post())

    @staticmethod
    async def patch(request):
        return dict()

    @staticmethod
    async def delete(request):
        return dict(await request.post())

    @staticmethod
    async def head(request):
        return dict()

    @staticmethod
    async def options(request):
        return dict()

    @staticmethod
    async def trace(request):
        return dict()
