class BaseValidator:
    def __init__(self):
        pass

    @staticmethod
    async def get(request, data):
        pass

    @staticmethod
    async def post(request, data):
        pass

    @staticmethod
    async def put(request, data):
        pass

    @staticmethod
    async def patch(request, data):
        pass

    @staticmethod
    async def delete(request, data):
        pass

    @staticmethod
    async def head(request, data):
        pass

    @staticmethod
    async def options(request, data):
        pass

    @staticmethod
    async def trace(request, data):
        pass
