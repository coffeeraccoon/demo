# For non-gunicorn deploy, just for debug
SERVER_HOST = "127.0.0.1"
SERVER_PORT = 8080

DB_DEBUG = False
DB_DRIVER = "mysql+pymysql"
DB_HOST = "127.0.0.1"
DB_PORT = "3306"
DB_USER = "asyncio_user"
DB_PASS = "asyncio_pass"
DB_NAME = "asyncio"

# To CMS_API, needs to remove
X_API_KEY = "secret123"

# TODO: Needs to autofill from CMS
WHITELIST_IP = ["127.0.0.1",
                "176.36.135.75",
                "195.137.167.42",
                "45.55.255.68"]

STATIC_FILES_ROOT = "D:\\gc-libs\\files\\"

# Not just a list of extension because maybe we need to decline some of specified filenames in future
# In validation each mask passed into the re.search() function
ACCEPTED_FILE_MASKS = [".jpg$",
                       ".jpeg$",
                       ".png$",
                       ".gif$",
                       ".js$",
                       ".css$",
                       ".html$",
                       ".htm$"]
# Out-of-date
MIN_FILE_SIZE = 0  # 0 bytes
MAX_FILE_SIZE = 524288000  # 500 Mb

MIN_REQUEST_SIZE = 0  # 0 bytes
MAX_REQUEST_SIZE = 524288000  # 500 Mb

AUTH_URL = "http://localhost:8081/auth"

DEFAULT_ACCESS_RULES = {
    "create_file": False,
    "read_file": True,
    "update_file": False,
    "delete_file": False,
    "create_dir": False,
    "read_dir": True,
    "update_dir": False,
    "delete_dir": False,
    "set_access_rule": False,
    "default": False
}
